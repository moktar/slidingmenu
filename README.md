Android-Sliding-Menu-With-Navigation-Drawer
===========================================

You might have noticed that lot of android applications introduced a sliding panel menu to navigate between major modules of the application. Previously this kind of UI was done using some third party libraries where a list view and some swiping gestures used to achieve this. But now android itself officially introduced sliding panel menu by introducing a newer concept called Navigation Drawer.

Most of the time Sliding Menu (Navigation Drawer) will be hidden and can be shown by swiping the screen from left edge to right or tapping the app icon on the action bar

how to use navigation drawer to add a sliding menu to your application.

In order to demonstrate navigation drawer, I am taking an example of Google+ navigation drawer and explained the process to achieve the same. But before starting the project I have downloaded required icons and using photoshop I have made each icon into different dimensions for xxhdpi (144×144 px), xhdpi (96×96 px), hdpi (72×72 px) and mdpi (48×48 px) drawbles.

Also I have downloaded navigation drawer toggle icon and included in drawable folders. You can get all the images in the source code of this tutorial. We need another image to replace the action bar up icon to toggle navigation drawer. Save following images and later add them to your project.

ic_drawer.png

ic_drawer ic_drawer ic_drawer

Now run the project and test the listview click event. You can see respected fragment is loading on selecting the list item. Following is the screenshot of my slider menu.

